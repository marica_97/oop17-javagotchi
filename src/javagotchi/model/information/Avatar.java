package javagotchi.model.information;

/**
 * @author giulia
 */
public enum Avatar {
    /**
     * Javagotchi's avatar is cat.
     */
    CAT,
    /**
     * Javagotchi's avatar is fox.
     */
    FOX,
    /**
     * Javagotchi's avatar is panda.
     */
    PANDA
}
